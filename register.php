<?php
	$pageTitle = "Register";
	include 'templateheader.php';
	
	if (isset($_GET['error']) && $_GET['error'] == 'nomatch')
	{
		echo '<p class = "error">Password didnt match</p>';
	}
	else if (isset($_GET['error']) && $_GET['error'] == 'exists')
	{
		echo '<p class = "error">User exists. Choose another username<p>';
	}
?>

<script>
	function check()
	{
		pass = document.getElementById('password').value;
		conf = document.getElementById('confirmpassword').value;
		
		if (pass != conf)
		{
			document.getElementById('passMatch').innerHTML = 'hasla nie pasuja';
		}
		else if (pass == conf)
			document.getElementById('passMatch').innerHTML = 'pasuje';
	}
</script>
<article>
	<form action = "finishregistration.php" method = "post">
		<table>
			<tr>
				<td><label for = "username">Username</label></td>
			</tr>
			<tr>
				<td><input type = "text" name = "username" id = "username" placeholder = "Username"/></td>
			</tr>
			<tr>
				<td><label for = "password">Password</label></td>
			</tr>
			<tr>
				<td><input type = "password" name = "password" id = "password" placeholder = "Password"/></td>
			</tr>
			<tr>
				<td><label for = "confirmpassword">Confirm password</label></td>
			</tr>
			<tr>
				<td><input type = "password" name = "confirmpassword" id = "confirmpassword" onkeyup = "check()" placeholder = "Confirm"/></td>
			</tr>
			<tr>
				<td class = "error" id = "passMatch"></td>
			</tr>
			<tr>
				<td><input type = "submit" value = "Register" class = "submit" id = "register" /></td>
			</tr>
		</table>
	</form>
</article>
<?php
	include 'templatefooter.php';
?>